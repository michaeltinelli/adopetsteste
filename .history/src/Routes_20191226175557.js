import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect, } from 'react-router-dom'
import Layout from './Layout/Layout'
import SessionPage from './pages/SessionPage';

function RouteWithLayout({layout, component, ...rest}){
    return (
      <Route exact {...rest} render={(props) =>
        React.createElement( layout, props, React.createElement(component, props)) 
      }/>
    );
}

export default function Routes() {
    return(
        <Router>
            <Switch>
                <RouteWithLayout path={'/'} layout={props => <Layout {...props} title={'Session'} />} 
                component={SessionPage} />
                
                <Redirect to='/' />
            </Switch>
        </Router>
    )
}