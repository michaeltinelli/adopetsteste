import React from 'react';
import logo from './logo.svg';
import './App.css';
import SessionPage from './pages/SessionPage';

function App() {
  return (
    <div className={'container'}>
      <SessionPage />
    </div>
  );
}

export default App;
