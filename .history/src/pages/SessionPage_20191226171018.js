import React from 'react'
import { InputDefault } from '../components/Inputs'
import { myKey } from '../consts/myKey'

export default ({ history }) => {


    return(
        <div>
            <InputDefault label={'API_KEY'} value={{...myKey}} />
        </div>
    )
}