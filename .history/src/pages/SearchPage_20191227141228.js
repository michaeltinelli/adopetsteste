import React,{ useState, useEffect } from 'react'
import { getSearchPetRequest } from '../services/services'
import Table from '../components/Table'



export function SearchPage({ history }) {

    const [pets, setPets] = useState([])
    const [columns, setColumns] = useState([])
    const [pagination, setPagination] = useState({})
    const [loading, setLoading] = useState(false)

    const accKeyBefore = localStorage.getItem('accessKey')

    useEffect(() => {
        async function getPets(params = {}) {
            const resp = await getSearchPetRequest(accKeyBefore, params)
            console.warn(resp)
            if(resp.status === 200 && resp.data.data.result.length > 0) {
                const { result } = resp.data.data
                await setPets(result)
                await loadColumns()
            }
        }
        getPets()

    },[])

    function loadColumns() {
        setColumns([
            { title: 'Name', dataIndex: 'name',
            sorter: (next, prev) => next.name.length - prev.name.length,
            sortDirections: ['ascend', 'descend'],
        },

            { title: 'Sex', dataIndex: 'sex_key', filters: [
                { text: 'Male', value: 'MALE'},
                { text: 'Female', value: 'FEMALE'},
            ],
                onFilter: (value, record) => record.sex_key.indexOf(value) === 0,
            },

            { title: 'Size', dataIndex: 'size_key', filters: [
                { text: 'Small', value: 'S'},
                { text: 'Medium', value: 'M'},
                { text: 'Large', value: 'L'},
                { text: 'Extra Large', value: 'XL'},
            ], 
                onFilter: (value, record) => record.size_key.indexOf(value) === 0,
            },

            { title: 'Age', dataIndex: 'age_key', filters: [
                { text: 'Baby', value: 'BABY'},
                { text: 'Young', value: 'YOUNG'},
                { text: 'Adult', value: 'ADULT'},
                { text: 'Senior', value: 'SENIOR'},
            ],
                onFilter: (value, record) => record.age_key.indexOf(value) === 0,
            }
        ])
    }

    function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
        
    }

    return(
       <div>
           <Table columns={columns} data={pets} onChange={onChange} 
           pagination={pagination} loading={loading} />
       </div>
    )
}