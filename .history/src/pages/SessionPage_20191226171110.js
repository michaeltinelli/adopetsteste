import React from 'react'
import { InputDefault } from '../components/Inputs'
import { myKey } from '../consts/myKey'

import { Panel } from 'primereact/panel'

export default ({ history }) => {


    return(
        <Panel header="Entrar" >
            <InputDefault label={'API_KEY'} value={{...myKey}} />
        </Panel>
    )
}