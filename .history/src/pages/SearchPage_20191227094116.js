import React,{ useState, useEffect } from 'react'
import { getSearchPetRequest } from '../services/services'
import Table from '../components/Table'

export function SearchPage({ history }) {

    const [pets, setPets] = useState([])
    const [columns, setColumns] = useState([])

    const accKeyBefore = localStorage.getItem('accessKey')

    useEffect(() => {
        async function getPets() {
            const resp = await getSearchPetRequest(accKeyBefore)
            console.warn(resp)
            if(resp.status === 200 && resp.data.data.result.length > 0) {
                const { result } = resp.data.data
                await setPets(result.map(p => {
                    const pet = {
                        name: p.name,
                        sex_key: p.sex_key,
                        size_key: p.size_key,
                        age_key: p.age_key
                    }
                    //console.log(pet)
                    return pet
                }))
               await loadColumns()
            }
        }
        getPets()

    },[])

    function loadColumns() {
        setColumns([
            { title: 'Name', dataIndex: 'name'},

            { title: 'Sex', dataIndex: 'sex', filters: [
                { text: 'Male', value: 'MALE'},
                { text: 'Female', value: 'FEMALE'},
            ]},

            { title: 'Size', dataIndex: 'size', filters: [
                { text: 'Small', value: 'S'},
                { text: 'Medium', value: 'M'},
                { text: 'Large', value: 'L'},
                { text: 'Extra Large', value: 'XL'},
            ], 
            onFilter: (value, record) => console.log(value, record),
            },

            { title: 'Age', dataIndex: 'age', filters: [
                { text: 'Baby', value: 'BABY'},
                { text: 'Young', value: 'YOUNG'},
                { text: 'Adult', value: 'ADULT'},
                { text: 'Senior', value: 'SENIOR'},
            ]}
        ])
    }

    return(
       <div>
           <Table columns={columns} data={pets} />
       </div>
    )
}