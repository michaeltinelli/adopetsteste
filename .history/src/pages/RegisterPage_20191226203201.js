import React,{ useState, useEffect } from 'react'
import { Form, Button } from 'antd'
import { InputDefault, InputPassword } from '../components/Inputs'
import { userData } from '../consts/user'
import { getRegisterRequest } from '../services/services'

export function RegisterPage({ history }) {

    const accKeyBefore = localStorage.getItem('accessKey')

    async function toSearchPage(e) {
        e.preventDefault();

        const resp = await getRegisterRequest(accKeyBefore)
        console.warn(resp)
        if(resp.status === 200) {
            const { access_key } = resp.data.data
        }
    }

    return(
       <Form onSubmit={toSearchPage}>
           <InputDefault label={'Email'} value={userData.organization_user.email} />
           <InputPassword label={'Password'} value={userData.organization_user.password} />
           <br/>
           <Button type="primary" htmlType="submit">Entrar</Button>
       </Form>
    )
}