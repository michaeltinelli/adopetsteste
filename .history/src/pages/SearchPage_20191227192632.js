import React,{ useState, useEffect } from 'react'
import { getSearchPetRequest } from '../services/services'
import Table from '../components/Table'
import Body from '../consts/body'

import { Button } from 'antd'
const ButtonGroup = Button.Group;

export function SearchPage({ history }) {

    const [pets, setPets] = useState([])
    const [columns, setColumns] = useState([])

    const [pagination, setPagination] = useState({})
    const [loading, setLoading] = useState(false)
    const [filters, setFilters] = useState({})
    const [sorters, setSorters] = useState({})

    const accKeyBefore = localStorage.getItem('accessKey')

    useEffect(() => {
        getPets(Body())
        
    },[])
    
    async function getPets(body) {
        const resp = await getSearchPetRequest(accKeyBefore, body)
        console.warn(resp)
        if(!resp) history.push('/ses')

        else {
            if(resp.data.status === 200 && resp.data.data) {
                const { result, pages, page } = resp.data.data
                await setPagination({ 
                    pages,
                    page,
                })
                await loadColumns()
                await setPets(result)
            } else {
                history.push('/ses')
            }
        }
    }

    function loadColumns() {
        setColumns([
            { title: 'Name', dataIndex: 'name',
            sorter: (next, prev) => next.name.length - prev.name.length,
            sortDirections: ['ascend', 'descend'],
        },

            { title: 'Sex', dataIndex: 'sex_key', filters: [
                { text: 'Male', value: 'MALE'},
                { text: 'Female', value: 'FEMALE'},
            ],
                onFilter: (value, record) => record.sex_key.indexOf(value) === 0,
            },

            { title: 'Size', dataIndex: 'size_key', filters: [
                { text: 'Small', value: 'S'},
                { text: 'Medium', value: 'M'},
                { text: 'Large', value: 'L'},
                { text: 'Extra Large', value: 'XL'},
            ], 
                onFilter: (value, record) => record.size_key.indexOf(value) === 0,
            },

            { title: 'Age', dataIndex: 'age_key', filters: [
                { text: 'Baby', value: 'BABY'},
                { text: 'Young', value: 'YOUNG'},
                { text: 'Adult', value: 'ADULT'},
                { text: 'Senior', value: 'SENIOR'},
            ],
                onFilter: (value, record) => record.age_key.indexOf(value) === 0,
            }
        ])
    }

    async function onChange(pagination, filters, sorter) {
        console.log('params', pagination, filters, sorter);
        await setPagination(pagination)
        if(sorter.order === 'ascend') setSorters("name")
        else  setSorters("-name")
        await getPets(Body(null, pagination.page, sorters))
        
    }

    return(
       <div>
           <ButtonGroup>
               <Button>Female</Button>
               <Button>Male</Button>
           </ButtonGroup>
           <hr/> 
           <Table columns={columns} data={pets} onChange={onChange} 
           pagination={pagination} loading={loading} />
       </div>
    )
}