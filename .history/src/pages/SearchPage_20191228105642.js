import React,{ useState, useEffect } from 'react'
import { getSearchPetRequest } from '../services/services'
import Table from '../components/Table'
import Body from '../consts/body'

import { Button } from 'antd'
const ButtonGroup = Button.Group;

export function SearchPage({ history }) {

    const [pets, setPets] = useState([])
    const [columns, setColumns] = useState([])

    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 0,
        total: 0,
    })

    const [loading, setLoading] = useState(false)
    const [sorters, setSorters] = useState([])
    const [sexy_key, setSexykey] = useState("FEMALE")

    const accKeyBefore = localStorage.getItem('accessKey')

    useEffect(() => {
        getPets(Body(sexy_key, pagination, sorters))
        
    },[])
    
    async function getPets(body) {
        console.log(body)
        const resp = await getSearchPetRequest(accKeyBefore, body)
        console.warn(resp)
        if(!resp) history.push('/ses')

        else {
            if(resp.data.status === 200 && resp.data.data) {
                const { result, page, pages } = resp.data.data
                await setPagination({
                    current: page,
                    total: pages * 10,
                    pageSize: result.lenght
                })
                await loadColumns()
                await setPets(result)
            } else {
                history.push('/ses')
            }
        }
    }

    function loadColumns() {
        setColumns([
            { title: 'Name', dataIndex: 'name',
            sorter: (next, prev) => next.name.length - prev.name.length,
            sortDirections: ['ascend', 'descend'],
        },

            { title: 'Sex', dataIndex: 'sex_key'},

            { title: 'Size', dataIndex: 'size_key', filters: [
                { text: 'Small', value: 'S'},
                { text: 'Medium', value: 'M'},
                { text: 'Large', value: 'L'},
                { text: 'Extra Large', value: 'XL'},
            ], 
                onFilter: (value, record) => record.size_key.indexOf(value) === 0,
            },

            { title: 'Age', dataIndex: 'age_key', filters: [
                { text: 'Baby', value: 'BABY'},
                { text: 'Young', value: 'YOUNG'},
                { text: 'Adult', value: 'ADULT'},
                { text: 'Senior', value: 'SENIOR'},
            ],
                onFilter: (value, record) => record.age_key.indexOf(value) === 0,
            }
        ])
    }

    async function onChange({ current }, filters, sorter) {
        console.log('params', current, filters, sorter);
        await setPagination({
            ...pagination,
            current
        })
        console.warn(pagination)
        
        if(!sorter.order) await setSorters([])
        else {
            await setSorters([sorter.order.localeCompare('ascend') ? 'name' : '-name'])
        }

        await getPets(Body(sexy_key, pagination, sorters))
        
    }


    async function loadPerSex(sex) {
        await setSexykey(sex)
        await getPets(Body(sex, pagination, sorters))
    }

    return(
       <div>
           <ButtonGroup>
               <Button onClick={() => loadPerSex('FEMALE') }>Female</Button>
               <Button onClick={() => loadPerSex('MALE') }>Male</Button>
           </ButtonGroup>
           <hr/> 
           <Table columns={columns} data={pets} onChange={onChange} 
           pagination={pagination} loading={loading} />
       </div>
    )
}