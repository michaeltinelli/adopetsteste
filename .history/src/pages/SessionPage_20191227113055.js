import React from 'react'
import { getSessionRequest } from '../services/services'

import { Button } from 'antd'


export function SessionPage({ history }) {

    async function toRegisterPage() {
        const resp = await getSessionRequest()
        //console.log(resp)
        if(resp.status === 200) {
            const { access_key } = resp.data.data
            localStorage.setItem('accessKey', access_key)
            history.push('/reg')
        }
    }

    return(
        <div>
            <Button type="primary" onClick={() => toRegisterPage()} >Prosseguir</Button>
        </div>
    )
}