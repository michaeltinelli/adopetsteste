import React,{ useState, useEffect } from 'react'
import { Form, Button } from 'antd'
import { InputDefault, InputPassword } from '../components/Inputs'
import { userData } from '../consts/user'
import { getRegisterRequest } from '../services/services'

export function RegisterPage({ history }) {

    const [accKeyBefore, setAccKeyBefore] = useState("")

    useEffect(() => {
        setAccKeyBefore(localStorage.getItem('accessKey'))
    }, [])

    async function toSearchPage(e) {
        e.preventDefault();

        const resp = await getRegisterRequest(accKeyBefore)
        console.warn(resp)
    }

    return(
       <Form onSubmit={toSearchPage}>
           <InputDefault label={'Email'} value={userData.organization_user.email} />
           <InputPassword label={'Password'} value={userData.organization_user.password} />
           <br/>
           <Button type="primary" htmlType="submit">Entrar</Button>
       </Form>
    )
}