import React from 'react'
import { Form } from 'antd'
import { InputDefault, InputPassword } from '../components/Inputs'
import { userData } from '../consts/user'

export default ({ history }) => {


    return(
       <Form>
           <InputDefault label={'Password'} value={userData.organization_user.email} />
           <InputPassword label={'Password'} value={userData.organization_user.password} />
       </Form>
    )
}