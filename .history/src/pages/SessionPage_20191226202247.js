import React from 'react'
import { getSessionRequest } from '../services/services'

import { Button } from 'antd'
import message from '../components/Message'

export function SessionPage({ history }) {

    async function toRegisterPage() {
        const resp = await getSessionRequest()
        console.log(resp)
        if(resp.status === 200) {
            const { access_key } = resp.data.data
            localStorage.setItem('accessKey', access_key)
            history.push('/reg')
        } else if(resp.status === 500) {
            message("Invalid Session")
        }
    }

    return(
        <div>
            <Button type="primary" onClick={() => toRegisterPage()} >Prosseguir</Button>
        </div>
    )
}