import React from 'react'
import { InputDefault } from '../components/Inputs'
import { myKey } from '../consts/myKey'

import { getSessionRequest } from '../services/services'

import { Panel } from 'primereact/panel'
import { Button } from 'primereact/button'

export default ({ history }) => {

    async function toRegisterPage() {
        const resp = await getSessionRequest()
        console.log(resp)
    }

    return(
        <Panel header="Entrar" >
            <InputDefault label={'API_KEY'} value={{...myKey}} />
            <br />
            <Button label={'Prosseguir'} className={'button'} onClick={() => toRegisterPage()} />
        </Panel>
    )
}