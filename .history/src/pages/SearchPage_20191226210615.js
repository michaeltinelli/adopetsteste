import React,{ useState, useEffect } from 'react'
import { Button } from 'antd'
import { getSearchPetRequest } from '../services/services'


export function SearchPage({ history }) {

    const [pets, setPets] = useState([])
    const accKeyBefore = localStorage.getItem('accessKey')

    useEffect(() => {
        async function getPets() {
            const resp = await getSearchPetRequest(accKeyBefore)
            console.warn(resp)
            
        }
    },[])

    return(
       <div>
           <Button type="primary" htmlType="submit">Entrar</Button>
       </div>
    )
}