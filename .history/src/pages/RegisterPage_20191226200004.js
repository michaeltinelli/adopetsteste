import React from 'react'
import { Form, Button } from 'antd'
import { InputDefault, InputPassword } from '../components/Inputs'
import { userData } from '../consts/user'

export function RegisterPage({ history }) {

    async function toSearchPage() {

    }

    return(
       <Form>
           <InputDefault label={'Email'} value={userData.organization_user.email} />
           <InputPassword label={'Password'} value={userData.organization_user.password} />
           <br/>
           <Button type="primary" htmlType="submit">Entrar</Button>
       </Form>
    )
}