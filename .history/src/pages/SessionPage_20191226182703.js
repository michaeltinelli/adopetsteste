import React from 'react'
import { getSessionRequest } from '../services/services'

import { Button } from 'antd'

export default ({ history }) => {

    async function toRegisterPage() {
        const resp = await getSessionRequest()
        console.log(resp)
        if(resp.status === 200) {
            history.push('/register')
        }
    }

    return(
        <div>
            <Button type="primary" onClick={() => toRegisterPage()} >Prosseguir</Button>
        </div>
    )
}