import React from 'react'

import { Layout } from 'antd'
const { Header, Content } = Layout

export default ({ children, title }) => {

    return(
        <Layout>
            <Header>{title}</Header>
            <Content>
                { children }
            </Content>
        </Layout>
    )
}