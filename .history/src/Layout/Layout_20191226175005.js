import React from 'react'
import { Route } from 'react-router-dom'

import { Layout } from 'antd'
const { Header, Content } = Layout

export default ({ layout , component, ...rest}) => {

    return(
        <Layout>
            <Header>{'Ado Pets'}</Header>
            <Content>
            {
                <Route exact {...rest} render={(props) =>
                    React.createElement( layout, props, React.createElement(component, props)) 
                } />
            }
            </Content>
        </Layout>
    )
}