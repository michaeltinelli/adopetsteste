import React from 'react'
import { Layout } from 'antd'

const { Header, Content } = Layout

export default ({ children }) => {

    return(
        <Layout>
            <Header>AdoPets</Header>
            <Content>{children}</Content>
        </Layout>
    )
}