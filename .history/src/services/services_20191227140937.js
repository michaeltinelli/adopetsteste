import axios from 'axios'
import { sessionRequest, petSearch, registerRequest } from '../consts/endpoints'
import { myKey } from '../consts/myKey'
import { userData } from '../consts/user'


export function getSessionRequest() {
    return axios.post(`${sessionRequest}`, { ...myKey })
}

export function getRegisterRequest(accessKey) {
    return axios.post(`${registerRequest}`, { ...userData }, {
        headers: {
            Authorization: `Bearer ${accessKey}`
        }
    })
}

export function getSearchPetRequest(accessKey, params) {
    return axios.post(`${petSearch}`, null, {
        headers: {
            Authorization: `Bearer ${accessKey}`
        },
        params
    })
}