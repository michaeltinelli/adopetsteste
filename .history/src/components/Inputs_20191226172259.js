import React from 'react'
import { InputText } from 'primereact/inputtext'

export const InputDefault = ({ label, value }) => {
    return(
        <>
            <label>{label}</label><br/>
            <InputText value={value} disabled readOnly />
        </>
    )
}