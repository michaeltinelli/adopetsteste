import React from 'react'
import { InputText } from 'primereact/inputtext'

export const InputDefault = ({ label, value }) => {
    return(
        <>
            <label>{label}</label>
            <InputText value={value}/>
        </>
    )
}