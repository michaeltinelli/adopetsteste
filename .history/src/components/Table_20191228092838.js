import React from 'react'
import { Table, Pagination} from 'antd'

export default ({ data, columns, onChange, pagination: { page, pages }, loading}) => {
    return(
        <Table columns={columns} rowKey={(v, i) => `${i}`} dataSource={data} onChange={onChange}
        pagination={<Pagination current={page} total={pages * 10} pageSize={pages}/>} loading={loading} />
    )
}