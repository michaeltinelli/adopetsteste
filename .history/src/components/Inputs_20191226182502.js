import React from 'react'
import { Input } from 'antd'

export const InputDefault = ({ label, value }) => {
    return(
        <>
            <label>{label}</label><br/>
            <Input disabled readOnly value={value} />
        </>
    )
}

export const InputPassword = ({ label, value }) => {
    return(
        <>
            <label>{label}</label><br/>
            <Input.Password disabled readOnly value={value} />
        </>
    )
}