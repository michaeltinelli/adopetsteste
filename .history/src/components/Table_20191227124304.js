import React from 'react'
import { Table } from 'antd'

export default ({ data, columns}) => {
    return(
        <Table bodyStyle={'table'} columns={columns} rowKey={(v, i) => `${i}`} dataSource={data} />
    )
}