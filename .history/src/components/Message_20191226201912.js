import React from 'react'
import { message } from 'antd'

export default ({ text }) => {
    message.error(text)
}