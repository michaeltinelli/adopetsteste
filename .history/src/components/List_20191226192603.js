import React from 'react'
import { List, Avatar } from 'antd'

export default ({ data, header }) => {
    return(
        <List header={header} dataSource={data} renderItem={item => (
            <List.Item>
                <List.Item.Meta avatar={<Avatar src={item.picture} />} title={item.name} />
            </List.Item>
        )} />
    )
}