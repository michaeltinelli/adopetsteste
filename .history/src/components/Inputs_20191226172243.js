import React from 'react'
import { InputText } from 'primereact/inputtext'

export const InputDefault = ({ label, value }) => {
    return(
        <div className={'container'}>
            <label>{label}</label>
            <InputText value={value} disabled readOnly />
        </div>
    )
}