import React from 'react'
import { Table, } from 'antd'

export default ({ data, columns, onChange, pagination, loading}) => {
    return(
        <Table columns={columns} rowKey={(v, i) => `${i}`} dataSource={data} onChange={onChange}
        pagination={pagination} loading={loading} />
    )
}