import React from 'react'
import { Table } from 'antd'

export default ({ data, columns, onChange}) => {
    return(
        <Table columns={columns} rowKey={(v, i) => `${i}`} dataSource={data} onChange={onChange} />
    )
}