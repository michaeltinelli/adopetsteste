import React from 'react'
import { Table, Pagination } from 'antd'

export default ({ data, columns, onChange, pagination, loading}) => {
    return(
        <Table columns={columns} rowKey={(v, i) => `${i}`} dataSource={data} onChange={onChange}
        pagination={<Pagination defaultCurrent={1} total={30} />} loading={loading} />
    )
}