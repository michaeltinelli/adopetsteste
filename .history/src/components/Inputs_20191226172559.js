import React from 'react'
import { InputText } from 'primereact/inputtext'

export const InputDefault = ({ label,  }) => {
    return(
        <>
            <label>{label}</label><br/>
            <InputText />
        </>
    )
}