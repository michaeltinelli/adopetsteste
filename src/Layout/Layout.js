import React from 'react'

import { Layout } from 'antd'
const { Header, Content } = Layout

export default ({ children, title }) => {

    return(
        <Layout>
            <Header className={'title'}>{title}</Header>
            <Content className={'container'}>
                { children }
            </Content>
        </Layout>
    )
}